import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Solution to the task 6.11 of RN_U6_Anwendungsschicht.
 * The URL points to a text file containing the 1.000 most often used words in German.
 * The DomainAssignmentChecker takes this list and checks how many of those words form a domain like "www.<word>.de".
 * and how many of them have got an assigned IP address.
 * 
 * @author Marwin Rieger 962450
 * @version 1.0
 *
 */
public class DomainAssignmentChecker {
	/**
	 * The URL of the list containing the 1.000 most often used words in German.
	 */
	private static final String DOMAINLIST = "http://wortschatz.uni-leipzig.de/Papers/top1000de.txt";
		
	public static void main(String[] args) {
		int assignmentCounter = 0;
		InetAddress currentAddress = null;
		URL domainListURL = null;
		BufferedReader domains = null;
		
		//first we'll try to get an URL object out of the URL String.
		try {
			domainListURL = new URL(DOMAINLIST);
		} catch (MalformedURLException e) {
			System.err.println("ERROR: Malformed URL has been inputted: " + e.getMessage());
			
			//we cannot do anything at all if we can't get the word list, so we take the exit right here.
			System.exit(-1);
		}
		
		//next we try to get a BufferedReader so we can read each word from the list.
		try {
			domains = new BufferedReader(new InputStreamReader(domainListURL.openStream()));
		} catch (IOException e) {
			System.err.println("ERROR: Couldn't open the file at the specified URL: " + e.getMessage());
			
			//we cannot do anything at all if we can't get the wordlist, so we take the exit right here.
			System.exit(-1);
		}
		
		//next thing to do is to check whether IP addresses have been assigned to the hosts.
		try {
			//just to tell the people out there that the program is running and doing stuff.
			System.out.println("Checking IP assignments...");
			
			String domainName;
			//we'll iterate over every word in the list.
			//so as long as there is a word in the list, we take it and check if there is an assignment.
			while ((domainName = domains.readLine()) != null) {
				try {
					//if the domain isn't assigned to an IP address the following method throws an UnkownHostException.
					//so as long as this doesn't happen there should be an assignment and we can increase the counter.
					currentAddress = InetAddress.getByName("www." + domainName + ".de");
					assignmentCounter++;
					
					//optional prints fyi
					System.out.println("--------------------");
					System.out.println("Host: " + currentAddress.getHostName());
					System.out.println("Address: " + currentAddress.getHostAddress());
					System.out.println("Counter: " + assignmentCounter);
					
				} catch (UnknownHostException e) {
					//enable these if you want to see which hosts are not assigned/cannot be resolved
					System.err.println("ERROR: Couldn't resolve host: " + e.getMessage());
				}
			}
			
			//close the BufferedReader, because we don't need it anymore.
			domains.close();
		} catch (IOException e) {
			System.err.println("ERROR: An I/O-Error occured: " + e.getMessage());
		}
		
		//print out the result.
		System.out.println();
		System.out.println(assignmentCounter + " addresses have been assigned so far");
	}

}
