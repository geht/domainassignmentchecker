A little command line tool developed for the module "Rechnernetze" in SS 2015.

The URL "http://wortschatz.uni-leipzig.de/Papers/top1000de.txt" points to a text file containing the 1.000 most often used words in German.

The DomainAssignmentChecker takes this list and checks how many of those words form a domain like "www.<word>.de" and how many of them have got an assigned IP address.

To setup this project just clone/fork it and import in eclipse.

You can get an executable jar file in the [download](https://bitbucket.org/geht/domainassignmentchecker/downloads) section.

To use the jar file just open up a command line and type in:

```
java -jar DomainAssignmentChecker.jar
```


just make sure you have java in your PATH variable or else you have to type in the absolute path to the java executable.

If you want to redirect the output you can do so like this:

```
java -jar DomainAssignmentChecker.jar > myfile
```
